package com.blankj.swipepanel.demo;

import com.blankj.swipepanel.SwipePanel;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.AttrHelper;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class BackActivity extends Ability {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "BackActivity");

    public static void start(Ability context) {
        Intent starter = new Intent();
        Operation operation =
                new Intent.OperationBuilder().withBundleName(context.getBundleName()).withAbilityName(BackActivity.class).build();
        starter.setOperation(operation);
        context.startAbility(starter);
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_back_activity);

        final SwipePanel swipePanel = new SwipePanel(this);
        swipePanel.setLeftEdgeSize(AttrHelper.vp2px(100, getContext()));// 设置左侧触发阈值 100dp
        swipePanel.setLeftDrawable(ResourceTable.Media_base_back);// 设置左侧 icon
        swipePanel.wrapView(findComponentById(ResourceTable.Id_rootLayout));// 设置嵌套在 rootLayout 外层
        swipePanel.setOnFullSwipeListener(new SwipePanel.OnFullSwipeListener() {// 设置完全划开松手后的监听
            @Override
            public void onFullSwipe(int direction) {
                terminateAbility();
                swipePanel.close(true);// 关闭
            }
        });
        swipePanel.setOnProgressChangedListener(new SwipePanel.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(int direction, float progress, boolean isTouch) {
                HiLog.error(LABEL, "" + progress);
            }
        });
    }
}
