package com.blankj.swipepanel.demo;

import com.blankj.swipepanel.SwipePanel;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.element.Element;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class LayoutSwipePanelActivity extends Ability {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "LayoutSwipePanelActivity");

    public static void start(Ability context) {
        Intent starter = new Intent();
        Operation operation =
                new Intent.OperationBuilder().withBundleName(context.getBundleName()).withAbilityName(LayoutSwipePanelActivity.class).build();
        starter.setOperation(operation);
        context.startAbility(starter);
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_layout_swipe_panel_activity);
        final SwipePanel swipePanel = (SwipePanel) findComponentById(ResourceTable.Id_swipePanel);
        swipePanel.setOnFullSwipeListener(new SwipePanel.OnFullSwipeListener() {
            @Override
            public void onFullSwipe(int direction) {
                new ToastDialog(LayoutSwipePanelActivity.this).setDuration(3500).setText(directionToString(direction))
                        .setAlignment(LayoutAlignment.BOTTOM).setOffset(0, 200).show();
                if (direction == SwipePanel.TOP) {
                    swipePanel.close(true);
                }
            }
        });
        swipePanel.setOnProgressChangedListener(new SwipePanel.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(int direction, float progress, boolean isTouch) {
                if (direction == SwipePanel.TOP) {
                    HiLog.info(LABEL, "progress = " + progress);
                    Element element = swipePanel.getTopDrawable();
                    element.setAlpha((int) (0.5f * (255 + progress * 255)));
                }
            }
        });
    }

    private static String directionToString(int direction) {
        switch (direction) {
            case SwipePanel.LEFT:
                return "left";
            case SwipePanel.TOP:
                return "top";
            case SwipePanel.RIGHT:
                return "right";
            default:
                return "bottom";
        }
    }
}
