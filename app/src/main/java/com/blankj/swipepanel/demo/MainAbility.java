package com.blankj.swipepanel.demo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);

        findComponentById(ResourceTable.Id_testLayoutSwipePanel).setClickedListener((c) -> {
            LayoutSwipePanelActivity.start(this);
        });

        findComponentById(ResourceTable.Id_testBack).setClickedListener((c) -> {
            BackActivity.start(this);
        });
    }
}
