package com.blankj.swipepanel.demo.junit;

import com.blankj.swipepanel.SwipePanel;
import com.blankj.swipepanel.demo.ResourceTable;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.Field;

public class SwipePanelTest {

    @Test
    public void testSetLeftSwipeColor() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        int color = Color.getIntColor("#ff0000");
        swipePanel.setLeftSwipeColor(color);
        Field field = SwipePanel.class.getDeclaredField("mPaintColor");
        field.setAccessible(true);
        int[] mPaintColor = (int[]) field.get(swipePanel);
        Assert.assertEquals(mPaintColor[SwipePanel.LEFT], color);
    }

    @Test
    public void testSetTopSwipeColor() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        int color = Color.getIntColor("#f0f000");
        swipePanel.setTopSwipeColor(color);
        Field field = SwipePanel.class.getDeclaredField("mPaintColor");
        field.setAccessible(true);
        int[] mPaintColor = (int[]) field.get(swipePanel);
        Assert.assertEquals(mPaintColor[SwipePanel.TOP], color);
    }

    @Test
    public void testSetRightSwipeColor() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        int color = Color.getIntColor("#0f00ff");
        swipePanel.setRightSwipeColor(color);
        Field field = SwipePanel.class.getDeclaredField("mPaintColor");
        field.setAccessible(true);
        int[] mPaintColor = (int[]) field.get(swipePanel);
        Assert.assertEquals(mPaintColor[SwipePanel.RIGHT], color);
    }

    @Test
    public void testSetBottomSwipeColor() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        int color = Color.getIntColor("#00ff00");
        swipePanel.setBottomSwipeColor(color);
        Field field = SwipePanel.class.getDeclaredField("mPaintColor");
        field.setAccessible(true);
        int[] mPaintColor = (int[]) field.get(swipePanel);
        Assert.assertEquals(mPaintColor[SwipePanel.BOTTOM], color);
    }

    @Test
    public void testSetLeftEdgeSize() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        swipePanel.setLeftEdgeSize(20);
        Field field = SwipePanel.class.getDeclaredField("mEdgeSizes");
        field.setAccessible(true);
        int[] mEdgeSizes = (int[]) field.get(swipePanel);
        Assert.assertEquals(mEdgeSizes[SwipePanel.LEFT], 20);
    }

    @Test
    public void testSetTopEdgeSize() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        swipePanel.setTopEdgeSize(20);
        Field field = SwipePanel.class.getDeclaredField("mEdgeSizes");
        field.setAccessible(true);
        int[] mEdgeSizes = (int[]) field.get(swipePanel);
        Assert.assertEquals(mEdgeSizes[SwipePanel.TOP], 20);
    }

    @Test
    public void testSetRightEdgeSize() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        swipePanel.setRightEdgeSize(20);
        Field field = SwipePanel.class.getDeclaredField("mEdgeSizes");
        field.setAccessible(true);
        int[] mEdgeSizes = (int[]) field.get(swipePanel);
        Assert.assertEquals(mEdgeSizes[SwipePanel.RIGHT], 20);
    }

    @Test
    public void testSetBottomEdgeSize() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        swipePanel.setBottomEdgeSize(20);
        Field field = SwipePanel.class.getDeclaredField("mEdgeSizes");
        field.setAccessible(true);
        int[] mEdgeSizes = (int[]) field.get(swipePanel);
        Assert.assertEquals(mEdgeSizes[SwipePanel.BOTTOM], 20);
    }

    @Test
    public void testSetLeftDrawable() throws IOException, NotExistException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        PixelMapElement pixelMapElement = new PixelMapElement(context.getResourceManager().getResource(ResourceTable.Media_base_back));
        swipePanel.setLeftDrawable(pixelMapElement);
        Element leftDrawable = swipePanel.getLeftDrawable();
        Assert.assertEquals(leftDrawable, pixelMapElement);
    }

    @Test
    public void testSetTopDrawable() throws IOException, NotExistException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        PixelMapElement pixelMapElement = new PixelMapElement(context.getResourceManager().getResource(ResourceTable.Media_base_back));
        swipePanel.setTopDrawable(pixelMapElement);
        Element topDrawable = swipePanel.getTopDrawable();
        Assert.assertEquals(topDrawable, pixelMapElement);
    }

    @Test
    public void testSetRightDrawable() throws IOException, NotExistException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        PixelMapElement pixelMapElement = new PixelMapElement(context.getResourceManager().getResource(ResourceTable.Media_base_back));
        swipePanel.setRightDrawable(pixelMapElement);
        Element rightDrawable = swipePanel.getRightDrawable();
        Assert.assertEquals(rightDrawable, pixelMapElement);
    }

    @Test
    public void testSetBottomDrawable() throws IOException, NotExistException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        PixelMapElement pixelMapElement = new PixelMapElement(context.getResourceManager().getResource(ResourceTable.Media_base_back));
        swipePanel.setBottomDrawable(pixelMapElement);
        Element bottomDrawable = swipePanel.getBottomDrawable();
        Assert.assertEquals(bottomDrawable, pixelMapElement);
    }

    @Test
    public void testSetLeftCenter() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        swipePanel.setLeftCenter(true);
        Field field = SwipePanel.class.getDeclaredField("mIsCenter");
        field.setAccessible(true);
        boolean[] mIsCenter = (boolean[]) field.get(swipePanel);
        Assert.assertEquals(true, mIsCenter[SwipePanel.LEFT]);
    }

    @Test
    public void testSetTopCenter() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        swipePanel.setTopCenter(true);
        Field field = SwipePanel.class.getDeclaredField("mIsCenter");
        field.setAccessible(true);
        boolean[] mIsCenter = (boolean[]) field.get(swipePanel);
        Assert.assertEquals(true, mIsCenter[SwipePanel.TOP]);
    }

    @Test
    public void testSetRightCenter() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        swipePanel.setRightCenter(true);
        Field field = SwipePanel.class.getDeclaredField("mIsCenter");
        field.setAccessible(true);
        boolean[] mIsCenter = (boolean[]) field.get(swipePanel);
        Assert.assertEquals(true, mIsCenter[SwipePanel.RIGHT]);
    }

    @Test
    public void testSetBottomCenter() throws IllegalAccessException, NoSuchFieldException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        swipePanel.setBottomCenter(true);
        Field field = SwipePanel.class.getDeclaredField("mIsCenter");
        field.setAccessible(true);
        boolean[] mIsCenter = (boolean[]) field.get(swipePanel);
        Assert.assertEquals(true, mIsCenter[SwipePanel.BOTTOM]);
    }

    @Test
    public void testSetLeftEnabled() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        swipePanel.setLeftEnabled(true);
        Field field = SwipePanel.class.getDeclaredField("mEnabled");
        field.setAccessible(true);
        boolean[] mEnabled = (boolean[]) field.get(swipePanel);
        Assert.assertEquals(true, mEnabled[SwipePanel.LEFT]);
    }

    @Test
    public void testSetTopEnabled() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        swipePanel.setTopEnabled(true);
        Field field = SwipePanel.class.getDeclaredField("mEnabled");
        field.setAccessible(true);
        boolean[] mEnabled = (boolean[]) field.get(swipePanel);
        Assert.assertEquals(true, mEnabled[SwipePanel.TOP]);
    }

    @Test
    public void testSetRightEnabled() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        swipePanel.setRightEnabled(true);
        Field field = SwipePanel.class.getDeclaredField("mEnabled");
        field.setAccessible(true);
        boolean[] mEnabled = (boolean[]) field.get(swipePanel);
        Assert.assertEquals(true, mEnabled[SwipePanel.RIGHT]);
    }

    @Test
    public void testSetBottomEnabled() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        swipePanel.setBottomCenter(true);
        Field field = SwipePanel.class.getDeclaredField("mEnabled");
        field.setAccessible(true);
        boolean[] mEnabled = (boolean[]) field.get(swipePanel);
        Assert.assertEquals(true, mEnabled[SwipePanel.BOTTOM]);
    }

    @Test
    public void testWrapView() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        int childCount = swipePanel.getChildCount();
        swipePanel.wrapView(new Component(context));
        Assert.assertEquals(childCount + 1, swipePanel.getChildCount());
    }

    @Test
    public void testSetOnFullSwipeListener() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        SwipePanel.OnFullSwipeListener onFullSwipeListener = new SwipePanel.OnFullSwipeListener() {
            @Override
            public void onFullSwipe(int direction) {
            }
        };
        swipePanel.setOnFullSwipeListener(onFullSwipeListener);
        Field field = SwipePanel.class.getDeclaredField("mListener");
        field.setAccessible(true);
        SwipePanel.OnFullSwipeListener mListener = (SwipePanel.OnFullSwipeListener) field.get(swipePanel);
        Assert.assertEquals(mListener, onFullSwipeListener);
    }

    @Test
    public void testSetOnProgressChangedListener() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        SwipePanel.OnProgressChangedListener onProgressChangedListener = new SwipePanel.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(int direction, float progress, boolean isTouch) {
            }
        };
        swipePanel.setOnProgressChangedListener(onProgressChangedListener);
        ;
        Field field = SwipePanel.class.getDeclaredField("mProgressListener");
        field.setAccessible(true);
        SwipePanel.OnProgressChangedListener mProgressListener = (SwipePanel.OnProgressChangedListener) field.get(swipePanel);
        Assert.assertEquals(mProgressListener, onProgressChangedListener);
    }

    @Test
    public void testIsOpen() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        Assert.assertEquals(false, swipePanel.isOpen(SwipePanel.LEFT));
    }

    @Test
    public void testClose() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SwipePanel swipePanel = new SwipePanel(context);
        swipePanel.close(true);
        Field field = SwipePanel.class.getDeclaredField("progresses");
        field.setAccessible(true);
        float[] progresses = (float[]) field.get(swipePanel);
        Assert.assertTrue(progresses[SwipePanel.LEFT] == 0);
    }
}