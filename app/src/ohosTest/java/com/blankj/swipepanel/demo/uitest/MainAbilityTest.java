package com.blankj.swipepanel.demo.uitest;

import com.blankj.swipepanel.SwipePanel;
import com.blankj.swipepanel.demo.MainAbility;
import com.blankj.swipepanel.demo.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Button;
import ohos.agp.utils.Rect;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.junit.*;
import org.junit.runners.MethodSorters;

import java.lang.reflect.Field;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MainAbilityTest {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x01214, "Test");
    private MainAbility mainAbility;

    @Before
    public void before() {
        mainAbility = EventHelper.startAbility(MainAbility.class);
        EventHelper.waitForActive(mainAbility, 5);
    }

    @After
    public void tearDown() {
        EventHelper.clearAbilities();
    }

    private void stopThread(int x) {
        try {
            Thread.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test01ReturnIcon() {
        stopThread(2000);
        Button button = (Button) mainAbility.findComponentById(ResourceTable.Id_testLayoutSwipePanel);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(mainAbility, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        SwipePanel swipePanel = (SwipePanel) currentTopAbility.findComponentById(ResourceTable.Id_swipePanel);
        EventHelper.inputSwipe(currentTopAbility, 50, 1600, 500, 1600, 1000);
        stopThread(2000);
        Assert.assertTrue("右滑图标未显示", swipePanel.isOpen(SwipePanel.LEFT));
    }

    @Test
    public void test02Icon() {
        stopThread(2000);
        Button button = (Button) mainAbility.findComponentById(ResourceTable.Id_testLayoutSwipePanel);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(mainAbility, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        SwipePanel swipePanel = (SwipePanel) currentTopAbility.findComponentById(ResourceTable.Id_swipePanel);
        EventHelper.inputSwipe(currentTopAbility, 540, 300, 540, 800, 1000);
        Assert.assertTrue("下滑图标未显示", swipePanel.isOpen(SwipePanel.TOP));
        stopThread(2000);
    }

    @Test
    public void test03dispaly() {
        stopThread(2000);
        Button button = (Button) mainAbility.findComponentById(ResourceTable.Id_testLayoutSwipePanel);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(mainAbility, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        SwipePanel swipePanel = (SwipePanel) currentTopAbility.findComponentById(ResourceTable.Id_swipePanel);
        EventHelper.inputSwipe(currentTopAbility, 50, 1600, 500, 1600, 1000);
        stopThread(2000);
        Assert.assertTrue("右滑图标未显示", swipePanel.isOpen(SwipePanel.LEFT));
        stopThread(5000);
        Assert.assertTrue("图标自动消失", swipePanel.isOpen(SwipePanel.LEFT));
    }

    @Test
    public void test04clear() {
        stopThread(2000);
        Button button = (Button) mainAbility.findComponentById(ResourceTable.Id_testLayoutSwipePanel);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(mainAbility, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        SwipePanel swipePanel = (SwipePanel) currentTopAbility.findComponentById(ResourceTable.Id_swipePanel);
        EventHelper.inputSwipe(currentTopAbility, 50, 1600, 500, 1600, 1000);
        stopThread(2000);
        Assert.assertTrue("右滑图标未显示", swipePanel.isOpen(SwipePanel.LEFT));
        EventHelper.inputSwipe(currentTopAbility, 540, 300, 540, 700, 1000);
        stopThread(2000);
        Assert.assertTrue("图标未能消失", !swipePanel.isOpen(SwipePanel.LEFT));
    }

    @Test
    public void test05height() {
        stopThread(2000);
        Button button = (Button) mainAbility.findComponentById(ResourceTable.Id_testLayoutSwipePanel);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(mainAbility, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        SwipePanel swipePanel = (SwipePanel) currentTopAbility.findComponentById(ResourceTable.Id_swipePanel);
        Rect mRect = null;
        try {
            Field f = SwipePanel.class.getDeclaredField("mRect");
            f.setAccessible(true);
            mRect = (Rect) f.get(swipePanel);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        EventHelper.inputSwipe(currentTopAbility, 50, 1600, 500, 1600, 1000);
        stopThread(2000);
        int height = (mRect.bottom + mRect.top) / 2;
        EventHelper.inputSwipe(currentTopAbility, 540, 300, 540, 700, 1000);
        stopThread(2000);
        EventHelper.inputSwipe(currentTopAbility, 50, 800, 500, 800, 1000);
        stopThread(2000);
        int height1 = (mRect.bottom + mRect.top) / 2;
        Assert.assertTrue("图标展开的高度不同", height != height1);

    }

    @Test
    public void test06width() {
        stopThread(2000);
        Button button = (Button) mainAbility.findComponentById(ResourceTable.Id_testLayoutSwipePanel);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(mainAbility, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        SwipePanel swipePanel = (SwipePanel) currentTopAbility.findComponentById(ResourceTable.Id_swipePanel);
        Rect mRect = null;
        try {
            Field f = SwipePanel.class.getDeclaredField("mRect");
            f.setAccessible(true);
            mRect = (Rect) f.get(swipePanel);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        EventHelper.inputSwipe(currentTopAbility, 200, 300, 200, 800, 1000);
        stopThread(2000);
        int width = (mRect.left + mRect.right) / 2;
        Assert.assertTrue("展开位置不是中轴线", width == 540);
    }

    @Test
    public void test07back() {
        stopThread(2000);
        Button button = (Button) mainAbility.findComponentById(ResourceTable.Id_testBack);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(mainAbility, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        Assert.assertTrue("页面跳转失败", mainAbility != currentTopAbility);
        EventHelper.inputSwipe(currentTopAbility, 50, 800, 500, 800, 1000);
        stopThread(2000);
        Ability currentTopAbility1 = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        Assert.assertTrue("右滑页面未返回", mainAbility == currentTopAbility1);
        stopThread(2000);
    }

}