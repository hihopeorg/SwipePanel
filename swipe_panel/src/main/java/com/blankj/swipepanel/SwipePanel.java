package com.blankj.swipepanel;

import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ComponentParent;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.multimodalinput.event.TouchEvent;

import java.io.IOException;

/**
 * <pre>
 *     author: blankj
 *     blog  : http://blankj.com
 *     time  : 2019/03/22
 *     desc  :
 * </pre>
 */
public class SwipePanel extends StackLayout implements Component.DrawTask, Component.TouchEventListener {

    private static final String TAG = "SwipePanel";

    public static final int LEFT = 0;
    public static final int TOP = 1;
    public static final int RIGHT = 2;
    public static final int BOTTOM = 3;

    private static final float TRIGGER_PROGRESS = 0.95f;

    private int mWidth;
    private int mHeight;

    private Paint mPaint;

    private float halfSize;
    private float unit;

    private int mTouchSlop;

    private Path[] mPath = new Path[4];
    private int[] mPaintColor = new int[4];
    private int[] mEdgeSizes = new int[4];
    private Element[] mDrawables = new Element[4];
    private int[][] mDrawableSizes = new int[4][2];
    private boolean[] mIsStart = new boolean[4];
    private float[] mDown = new float[4];
    private float[] progresses = new float[4];
    private float[] preProgresses = new float[4];
    private boolean[] mIsCenter = new boolean[4];
    private boolean[] mEnabled = {true, true, true, true};

    private float mDownX;
    private float mDownY;
    private float mCurrentX;
    private float mCurrentY;
    private Rect mRect = new Rect();

    private boolean mIsEdgeStart;
    private int mStartDirection = -1;

    private int mLimit;

    private OnFullSwipeListener mListener;

    private OnProgressChangedListener mProgressListener;

    public SwipePanel(Context context) {
        this(context, null);
    }

    public SwipePanel(Context context, AttrSet attrs) {
        super(context, attrs);
        int edgeSlop = AttrHelper.vp2px(12, getContext());
        mTouchSlop = AttrHelper.vp2px(8, getContext());

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setStyle(Paint.Style.FILL_STYLE);

        halfSize = dp2px(72);
        unit = halfSize / 16;

        if (attrs != null) {
            setLeftSwipeColor(ResourceHelper.getColor(attrs, "leftSwipeColor", Color.BLACK).getValue());
            setTopSwipeColor(ResourceHelper.getColor(attrs, "topSwipeColor", Color.BLACK).getValue());
            setRightSwipeColor(ResourceHelper.getColor(attrs, "rightSwipeColor", Color.BLACK).getValue());
            setBottomSwipeColor(ResourceHelper.getColor(attrs, "bottomSwipeColor", Color.BLACK).getValue());

            setLeftEdgeSize(ResourceHelper.getDimension(attrs, "leftEdgeSize", edgeSlop));
            setTopEdgeSize(ResourceHelper.getDimension(attrs, "topEdgeSize", edgeSlop));
            setRightEdgeSize(ResourceHelper.getDimension(attrs, "rightEdgeSize", edgeSlop));
            setBottomEdgeSize(ResourceHelper.getDimension(attrs, "bottomEdgeSize", edgeSlop));

            setLeftDrawable(ResourceHelper.getElement(attrs, "leftDrawable"));
            setTopDrawable(ResourceHelper.getElement(attrs, "topDrawable"));
            setRightDrawable(ResourceHelper.getElement(attrs, "rightDrawable"));
            setBottomDrawable(ResourceHelper.getElement(attrs, "bottomDrawable"));

            setLeftCenter(ResourceHelper.getBool(attrs, "isLeftCenter", false));
            setTopCenter(ResourceHelper.getBool(attrs, "isTopCenter", false));
            setRightCenter(ResourceHelper.getBool(attrs, "isRightCenter", false));
            setBottomCenter(ResourceHelper.getBool(attrs, "isBottomCenter", false));

            setLeftEnabled(ResourceHelper.getBool(attrs, "isLeftEnabled", true));
            setTopEnabled(ResourceHelper.getBool(attrs, "isTopEnabled", true));
            setRightEnabled(ResourceHelper.getBool(attrs, "isRightEnabled", true));
            setBottomEnabled(ResourceHelper.getBool(attrs, "isBottomEnabled", true));
        }
        addDrawTask(this);
        setTouchEventListener(this);
    }

    public void setLeftSwipeColor(int color) {
        setSwipeColor(color, LEFT);
    }

    public void setTopSwipeColor(int color) {
        setSwipeColor(color, TOP);
    }

    public void setRightSwipeColor(int color) {
        setSwipeColor(color, RIGHT);
    }

    public void setBottomSwipeColor(int color) {
        setSwipeColor(color, BOTTOM);
    }

    private void setSwipeColor(int color, int direction) {
        mPaintColor[direction] = color;
    }

    public void setLeftEdgeSize(int size) {
        mEdgeSizes[LEFT] = size;
    }

    public void setTopEdgeSize(int size) {
        mEdgeSizes[TOP] = size;
    }

    public void setRightEdgeSize(int size) {
        mEdgeSizes[RIGHT] = size;
    }

    public void setBottomEdgeSize(int size) {
        mEdgeSizes[BOTTOM] = size;
    }

    public void setLeftDrawable(int drawableId) {
        setDrawable(drawableId, LEFT);
    }

    public void setTopDrawable(int drawableId) {
        setDrawable(drawableId, TOP);
    }

    public void setRightDrawable(int drawableId) {
        setDrawable(drawableId, RIGHT);
    }

    public void setBottomDrawable(int drawableId) {
        setDrawable(drawableId, BOTTOM);
    }

    private void setDrawable(int drawableId, int direction) {
        mDrawables[direction] = getDrawable(getContext(), drawableId);
        if (mDrawables[direction] != null) {
            mDrawableSizes[direction][0] = mDrawables[direction].getWidth();
            mDrawableSizes[direction][1] = mDrawables[direction].getHeight();
        }
    }

    public void setLeftDrawable(Element drawable) {
        setDrawable(drawable, LEFT);
    }

    public void setTopDrawable(Element drawable) {
        setDrawable(drawable, TOP);
    }

    public void setRightDrawable(Element drawable) {
        setDrawable(drawable, RIGHT);
    }

    public void setBottomDrawable(Element drawable) {
        setDrawable(drawable, BOTTOM);
    }

    private void setDrawable(Element drawable, int direction) {
        mDrawables[direction] = drawable;
        if (mDrawables[direction] != null) {
            mDrawableSizes[direction][0] = mDrawables[direction].getWidth();
            mDrawableSizes[direction][1] = mDrawables[direction].getHeight();
        }
    }

    public Element getLeftDrawable() {
        return mDrawables[LEFT];
    }

    public Element getTopDrawable() {
        return mDrawables[TOP];
    }

    public Element getRightDrawable() {
        return mDrawables[RIGHT];
    }

    public Element getBottomDrawable() {
        return mDrawables[BOTTOM];
    }

    public void setLeftCenter(boolean isCenter) {
        setCenter(isCenter, LEFT);
    }

    public void setTopCenter(boolean isCenter) {
        setCenter(isCenter, TOP);
    }

    public void setRightCenter(boolean isCenter) {
        setCenter(isCenter, RIGHT);
    }

    public void setBottomCenter(boolean isCenter) {
        setCenter(isCenter, BOTTOM);
    }

    private void setCenter(boolean isCenter, int direction) {
        mIsCenter[direction] = isCenter;
    }

    public void setLeftEnabled(boolean enabled) {
        setEnabled(enabled, LEFT);
    }

    public void setTopEnabled(boolean enabled) {
        setEnabled(enabled, TOP);
    }

    public void setRightEnabled(boolean enabled) {
        setEnabled(enabled, RIGHT);
    }

    public void setBottomEnabled(boolean enabled) {
        setEnabled(enabled, BOTTOM);
    }

    private void setEnabled(boolean enabled, int direction) {
        mEnabled[direction] = enabled;
    }

    public void wrapView(Component view) {
        ComponentParent parent = view.getComponentParent();
        if (parent instanceof ComponentContainer) {
            ComponentContainer group = (ComponentContainer) parent;
            int i = group.getChildIndex(view);
            ComponentContainer.LayoutConfig layoutParams = view.getLayoutConfig();
            group.removeComponentAt(i);
            group.addComponent(this, i, layoutParams);
            addComponent(view, ComponentContainer.LayoutConfig.MATCH_PARENT,
                    ComponentContainer.LayoutConfig.MATCH_PARENT);
        } else {
            addComponent(view, ComponentContainer.LayoutConfig.MATCH_PARENT,
                    ComponentContainer.LayoutConfig.MATCH_PARENT);
        }
    }

    public void setOnFullSwipeListener(OnFullSwipeListener listener) {
        mListener = listener;
    }

    public void setOnProgressChangedListener(OnProgressChangedListener listener) {
        mProgressListener = listener;
    }

    public boolean isOpen(int direction) {
        return progresses[direction] >= TRIGGER_PROGRESS;
    }

    public void close() {
        close(true);
    }

    public void close(int direction) {
        close(direction, true);
    }

    public void close(boolean isAnim) {
        if (isAnim) {
            animClose();
        } else {
            progresses[LEFT] = 0;
            progresses[TOP] = 0;
            progresses[RIGHT] = 0;
            progresses[BOTTOM] = 0;
            postInvalidate();
        }
    }

    public void close(int direction, boolean isAnim) {
        if (isAnim) {
            animClose(direction);
        } else {
            progresses[direction] = 0;
            postInvalidate();
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mWidth = getEstimatedWidth();
        mHeight = getEstimatedHeight();
        mLimit = Math.min(mWidth, mHeight) / 3;
        drawPath(canvas);
    }

    private void drawPath(Canvas canvas) {
        drawPath(canvas, LEFT);
        drawPath(canvas, TOP);
        drawPath(canvas, RIGHT);
        drawPath(canvas, BOTTOM);
    }

    private void drawPath(Canvas canvas, int direction) {
        if (mPath[direction] == null || progresses[direction] <= 0) {
            return;
        }
        updatePaint(direction);
        canvas.drawPath(getPath(direction), mPaint);
        drawIcon(canvas, direction);
    }

    private Path getPath(int direction) {
        if (preProgresses[direction] != progresses[direction]) {
            mPath[direction].reset();
            float edge, pivot = mDown[direction];
            int mark;
            if (direction == LEFT) {
                edge = 0;
                mark = 1;
            } else if (direction == TOP) {
                edge = 0;
                mark = 1;
            } else if (direction == RIGHT) {
                edge = mWidth;
                mark = -1;
            } else {
                edge = mHeight;
                mark = -1;
            }
            if (direction == LEFT || direction == RIGHT) {
                curPathX = edge;
                curPathY = pivot - halfSize;
            } else {
                curPathX = pivot - halfSize;
                curPathY = edge;
            }
            mPath[direction].moveTo(curPathX, curPathY);

            quad(edge, pivot - halfSize, direction);
            quad(edge + progresses[direction] * unit * mark, pivot - halfSize + 5 * unit, direction);
            quad(edge + progresses[direction] * 10 * unit * mark, pivot, direction);
            quad(edge + progresses[direction] * unit * mark, pivot + halfSize - 5 * unit, direction);
            quad(edge, pivot + halfSize, direction);
            quad(edge, pivot + halfSize, direction);
        }
        return mPath[direction];
    }

    private void drawIcon(Canvas canvas, int direction) {
        if (mDrawables[direction] == null) {
            return;
        }
        int dWidth = mDrawableSizes[direction][0];
        int dHeight = mDrawableSizes[direction][1];
        int fitSize = (int) (progresses[direction] * 5 * unit);

        int width, height, deltaWidth = 0, deltaHeight = 0;

        if (dWidth >= dHeight) {
            width = fitSize;
            height = width * dHeight / dWidth;
            deltaHeight = fitSize - height;
        } else {
            height = fitSize;
            width = height * dWidth / dHeight;
            deltaWidth = fitSize - width;
        }

        if (direction == LEFT) {
            mRect.left = (int) (0 + progresses[direction] * unit * 1 + deltaWidth / 2 * 1);
            mRect.top = (int) (mDown[LEFT] - height / 2);
            mRect.right = mRect.left + width;
            mRect.bottom = mRect.top + height;
        } else if (direction == RIGHT) {
            mRect.right = (int) (mWidth + progresses[direction] * unit * -1 + deltaWidth / 2 * -1);
            mRect.top = (int) (mDown[RIGHT] - height / 2f);
            mRect.left = mRect.right - width;
            mRect.bottom = mRect.top + height;
        } else if (direction == TOP) {
            mRect.left = (int) (mDown[TOP] - width / 2);
            mRect.top = (int) (0 + progresses[direction] * unit * 1 + deltaHeight / 2 * 1);
            mRect.right = mRect.left + width;
            mRect.bottom = mRect.top + height;
        } else {
            mRect.left = (int) (mDown[BOTTOM] - width / 2);
            mRect.bottom = (int) (mHeight + progresses[direction] * unit * -1 + deltaHeight / 2 * -1);
            mRect.top = mRect.bottom - height;
            mRect.right = mRect.left + width;
        }
        mDrawables[direction].setBounds(mRect);
        mDrawables[direction].drawToCanvas(canvas);
    }

    private void quad(float pathX, float pathY, int direction) {
        float preX = curPathX;
        float preY = curPathY;
        if (direction == LEFT || direction == RIGHT) {
            curPathX = pathX;
            curPathY = pathY;
        } else {
            //noinspection SuspiciousNameCombination
            curPathX = pathY;
            //noinspection SuspiciousNameCombination
            curPathY = pathX;
        }
        mPath[direction].quadTo(preX, preY, (preX + curPathX) / 2, (preY + curPathY) / 2);
    }

    private float curPathX;
    private float curPathY;

    private void updatePaint(int direction) {
        mPaint.setColor(new Color(mPaintColor[direction]));
        float alphaProgress = progresses[direction];
        if (alphaProgress < 0.25f) {
            alphaProgress = 0.25f;
        } else if (alphaProgress > 0.75f) {
            alphaProgress = 0.75f;
        }
        mPaint.setAlpha((int) (alphaProgress * 255));
    }

    private void animClose() {
        animClose(LEFT);
        animClose(TOP);
        animClose(RIGHT);
        animClose(BOTTOM);
    }

    private void animClose(final int direction) {
        if (progresses[direction] > 0) {
            final AnimatorValue anim = new AnimatorValue();
            final float start = progresses[direction];
            final float end = 0;
            anim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    float value = start + (end - start) * v;
                    progresses[direction] = value;
                    if (mProgressListener != null) {
                        mProgressListener.onProgressChanged(direction, progresses[direction], false);
                    }
                    postInvalidate();
                }
            });
            anim.setDuration(100);
            anim.start();
        }
    }


    @Override
    public boolean onTouchEvent(Component component, TouchEvent ev) {
        int action = ev.getAction();
        if (action == TouchEvent.PRIMARY_POINT_DOWN) {
            mDownX = ev.getPointerPosition(ev.getIndex()).getX();
            mDownY = ev.getPointerPosition(ev.getIndex()).getY();
            mIsStart[LEFT] = mEnabled[LEFT] && mDrawables[LEFT] != null && !isOpen(LEFT) && mDownX <= mEdgeSizes[LEFT];
            mIsStart[TOP] = mEnabled[TOP] && mDrawables[TOP] != null && !isOpen(TOP) && mDownY <= mEdgeSizes[TOP];
            mIsStart[RIGHT] =
                    mEnabled[RIGHT] && mDrawables[RIGHT] != null && !isOpen(RIGHT) && mDownX >= getWidth() - mEdgeSizes[RIGHT];
            mIsStart[BOTTOM] =
                    mEnabled[BOTTOM] && mDrawables[BOTTOM] != null && !isOpen(BOTTOM) && mDownY >= getHeight() - mEdgeSizes[BOTTOM];
            mIsEdgeStart = mIsStart[LEFT] || mIsStart[TOP] || mIsStart[RIGHT] || mIsStart[BOTTOM];
            if (mIsEdgeStart) {
                mStartDirection = -1;
            }
            return true;
        }
        if (mIsEdgeStart) {
            if (action == TouchEvent.POINT_MOVE) {
                mCurrentX = ev.getPointerPosition(ev.getIndex()).getX();
                mCurrentY = ev.getPointerPosition(ev.getIndex()).getY();
                if (mStartDirection == -1) {
                    float deltaX = mCurrentX - mDownX;
                    float deltaY = mCurrentY - mDownY;
                    float disX = Math.abs(deltaX);
                    float disY = Math.abs(deltaY);
                    if (disX > mTouchSlop || disY > mTouchSlop) {
                        if (disX >= disY) {
                            if (mIsStart[LEFT] && deltaX > 0) {
                                decideDirection(LEFT);
                            } else if (mIsStart[RIGHT] && deltaX < 0) {
                                decideDirection(RIGHT);
                            }
                        } else {
                            if (mIsStart[TOP] && deltaY > 0) {
                                decideDirection(TOP);
                            } else if (mIsStart[BOTTOM] && deltaY < 0) {
                                decideDirection(BOTTOM);
                            }
                        }
                    }
                }
                if (mStartDirection != -1) {
                    float preProgress = preProgresses[mStartDirection];
                    preProgresses[mStartDirection] = progresses[mStartDirection];
                    progresses[mStartDirection] = calculateProgress();
                    if (Math.abs(preProgress - progresses[mStartDirection]) > 0.01) {
                        postInvalidate();
                        if (mProgressListener != null) {
                            mProgressListener.onProgressChanged(mStartDirection, progresses[mStartDirection], true);
                        }
                    } else {
                        preProgresses[mStartDirection] = preProgress;
                    }
                }
            } else if (action == TouchEvent.PRIMARY_POINT_UP || action == TouchEvent.CANCEL) {
                if (mStartDirection != -1) {
                    mCurrentX = ev.getPointerPosition(ev.getIndex()).getX();
                    mCurrentY = ev.getPointerPosition(ev.getIndex()).getY();
                    progresses[mStartDirection] = calculateProgress();
                    if (isOpen(mStartDirection)) {
                        if (mListener != null) {
                            mListener.onFullSwipe(mStartDirection);
                        }
                    } else {
                        close(mStartDirection, true);
                    }
                }
            }
        }
        return true;
    }

    private void decideDirection(int direction) {
        if (direction == LEFT || direction == RIGHT) {
            if (mIsCenter[direction]) {
                mDown[direction] = mHeight / 2f;
            } else {
                if (mDownY < halfSize) {
                    mDown[direction] = halfSize;
                } else if (mDownY >= mHeight - halfSize) {
                    mDown[direction] = mHeight - halfSize;
                } else {
                    mDown[direction] = mDownY;
                }
            }
        } else {
            if (mIsCenter[direction]) {
                mDown[direction] = mWidth / 2f;
            } else {
                if (mDownX < halfSize) {
                    mDown[direction] = halfSize;
                } else if (mDownX >= mWidth - halfSize) {
                    mDown[direction] = mWidth - halfSize;
                } else {
                    mDown[direction] = mDownX;
                }
            }
        }
        mStartDirection = direction;
        if (mPath[direction] == null) {
            mPath[direction] = new Path();
        }
        preProgresses[direction] = 0;
        cancelChildViewTouch();
        //requestDisallowInterceptTouchEvent(true);
    }

    private float calculateProgress() {
        if (mStartDirection == LEFT) {
            float deltaX = mCurrentX - mDownX;
            if (deltaX <= 0) {
                return 0;
            }
            return Math.min(deltaX / mLimit, 1);
        } else if (mStartDirection == TOP) {
            float deltaY = mCurrentY - mDownY;
            if (deltaY <= 0) {
                return 0;
            }
            return Math.min(deltaY / mLimit, 1);
        } else if (mStartDirection == RIGHT) {
            float deltaX = mCurrentX - mDownX;
            if (deltaX >= 0) {
                return 0;
            }
            return Math.min(-deltaX / mLimit, 1);
        } else {
            float deltaY = mCurrentY - mDownY;
            if (deltaY >= 0) {
                return 0;
            }
            return Math.min(-deltaY / mLimit, 1);
        }
    }

    private void cancelChildViewTouch() {
        // Ohos has no dispatchTouchEvent method, children always deal with event first, no need to control
        // children's cancel.
    }

    private int dp2px(final float dpValue) {
        return AttrHelper.vp2px(dpValue, getContext());
    }

    private static final Object LOCK = new Object();

    private static Element getDrawable(Context context, int id) {
        Element result = null;
        try {
            result = ElementScatter.getInstance(context).parse(id);
        } catch (Exception e) {
        }
        if (result == null) {
            try {
                result = new PixelMapElement(context.getResourceManager().getResource(id));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public interface OnFullSwipeListener {
        void onFullSwipe(int direction);
    }

    public interface OnProgressChangedListener {
        void onProgressChanged(int direction, float progress, boolean isTouch);
    }

    private void postInvalidate() {
        getContext().getUITaskDispatcher().asyncDispatch(this::invalidate);
    }
}
