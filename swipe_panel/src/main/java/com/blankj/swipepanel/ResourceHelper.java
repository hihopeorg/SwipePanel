/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blankj.swipepanel;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.ElementScatterException;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;

public class ResourceHelper {

    public static boolean getBool(AttrSet attrSet, String name, boolean defaultValue) {
        boolean result = defaultValue;
        if (attrSet != null && attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getBoolValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static boolean getBool(int resId, Context context, boolean defaultValue) {
        try {
            return context.getResourceManager().getElement(resId).getBoolean();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return defaultValue;
    }

    public static int getInt(AttrSet attrSet, String name, int defaultValue) {
        int result = defaultValue;
        if (attrSet != null && attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getIntegerValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static int getInt(int resId, Context context, int defaultValue) {
        try {
            return context.getResourceManager().getElement(resId).getInteger();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return defaultValue;
    }

    public static int getResourceId(AttrSet attrSet, String name, int defaultValue) {
        int result = defaultValue;
        if (attrSet != null && attrSet.getAttr(name).isPresent()) {
            String strValue = attrSet.getAttr(name).get().getStringValue();
            if (strValue.indexOf(":") != -1) {
                result = Integer.parseInt(strValue.substring(strValue.lastIndexOf(":") + 1));
            }
        }
        return result;
    }

    public static long getLong(AttrSet attrSet, String name, long defaultValue) {
        long result = defaultValue;
        if (attrSet != null && attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getLongValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static float getFloat(AttrSet attrSet, String name, float defaultValue) {
        float result = defaultValue;
        if (attrSet != null && attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getFloatValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static float getFloat(int resId, Context context, float defaultValue) {
        try {
            return context.getResourceManager().getElement(resId).getFloat();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return defaultValue;
    }

    public static String getString(AttrSet attrSet, String name, String defaultValue) {
        String result = defaultValue;
        if (attrSet != null && attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getStringValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static String getString(int resId, Context context, String defaultValue) {
        try {
            return context.getResourceManager().getElement(resId).getString();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return defaultValue;
    }

    public static int getDimension(AttrSet attrSet, String name, int defaultValue) {
        int result = defaultValue;
        if (attrSet != null && attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getDimensionValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static int getDimensionFromPx(int resId, Context context, int defaultValue) {
        try {
            return (int) (context.getResourceManager().getElement(resId).getFloat() + 0.5f);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return defaultValue;
    }

    public static int getDimensionFromVp(int resId, Context context, int defaultValue) {
        try {
            return AttrHelper.vp2px(context.getResourceManager().getElement(resId).getFloat(), context);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return defaultValue;
    }

    public static int getDimensionFromFp(int resId, Context context, int defaultValue) {
        try {
            return AttrHelper.fp2px(context.getResourceManager().getElement(resId).getFloat(), context);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return defaultValue;
    }

    public static Color getColor(AttrSet attrSet, String name, Color defaultValue) {
        Color result = defaultValue;
        if (attrSet != null && attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getColorValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static Color getColor(int resId, Context context, Color defaultValue) {
        try {
            return new Color(context.getResourceManager().getElement(resId).getColor());
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return defaultValue;
    }

    public static int getColorV(AttrSet attrSet, String name, int defaultValue) {
        int result = defaultValue;
        if (attrSet != null && attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getColorValue().getValue();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static int getColorV(int resId, Context context, int defaultValue) {
        try {
            return context.getResourceManager().getElement(resId).getColor();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return defaultValue;
    }

    public static Element getElement(AttrSet attrSet, String name) {
        return getElement(attrSet, name, null);
    }

    public static Element getElement(AttrSet attrSet, String name, Element defaultValue) {
        Element result = defaultValue;
        if (attrSet != null && attrSet.getAttr(name).isPresent()) {
            try {
                result = attrSet.getAttr(name).get().getElement();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static Element getElement(int resId, Context context, Element defaultValue) {
        Element result = defaultValue;
        try {
            result = ElementScatter.getInstance(context).parse(resId);
        } catch (ElementScatterException e) {// resId doesn't reference to a xml file, maybe is a image file
            try {
                ImageSource imageSource = ImageSource.create(context.getResourceManager().getResource(resId),
                        new ImageSource.SourceOptions());
                PixelMap pixelMap = imageSource.createPixelmap(new ImageSource.DecodingOptions());
                result = new PixelMapElement(pixelMap);
            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (NotExistException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }
}
