# SwipePanel

**本项目是基于开源项目 SwipePanel 进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/Blankj/SwipePanel ）追踪到原项目版本**

#### 项目介绍

- 项目名称：SwipePanel
- 所属系列：ohos的第三方组件适配移植
- 功能：支持界面侧滑返回的视图控件
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 无原项目基线版本
- 调用差异：无
- 原项目Doc地址：https://github.com/Blankj/SwipePanel
- 编程语言：Java 



#### 效果演示
<img src="gif/SwipePanel1.gif"/>

<img src="gif/SwipePanel2.gif"/>

#### 安装教程
方法1. 
1. 将本三方库的har下载。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在entry级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```
方法2. 

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.blankj.ohos:swipe-panel:1.0.0'
}
```

#### 使用说明

代码使用
```
 final SwipePanel swipePanel = new SwipePanel(this);
        swipePanel.setLeftEdgeSize(AttrHelper.vp2px(100, getContext()));// 设置左侧触发阈值 100dp
        swipePanel.setLeftDrawable(ResourceTable.Media_base_back);// 设置左侧 icon
        swipePanel.wrapView(findComponentById(ResourceTable.Id_rootLayout));// 设置嵌套在 rootLayout 外层
        swipePanel.setOnFullSwipeListener(new SwipePanel.OnFullSwipeListener() {// 设置完全划开松手后的监听
            @Override
            public void onFullSwipe(int direction) {
                terminateAbility();
                swipePanel.close(true);// 关闭
            }
        });
        swipePanel.setOnProgressChangedListener(new SwipePanel.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(int direction, float progress, boolean isTouch) {
                HiLog.error(LABEL, "" + progress);
            }
        });
```
xml使用
```
<?xml version="1.0" encoding="utf-8"?>
<com.blankj.swipepanel.SwipePanel
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:app="http://schemas.ohos.com/apk/res-auto"
    ohos:id="$+id:swipePanel"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:background_element="$color:mediumGray"
    app:bottomDrawable="$media:icon"
    app:bottomEdgeSize="100vp"
    app:bottomSwipeColor="$color:colorAccent"
    app:isBottomCenter="true"
    app:isLeftCenter="false"
    app:isRightCenter="false"
    app:isTopCenter="true"
    app:leftDrawable="$media:base_back"
    app:leftEdgeSize="100vp"
    app:leftSwipeColor="$color:colorPrimary"
    app:rightDrawable="$media:base_back"
    app:rightEdgeSize="100vp"
    app:rightSwipeColor="$color:colorPrimary"
    app:topDrawable="$media:icon"
    app:topEdgeSize="100vp"
    app:topSwipeColor="$color:colorAccent">

    <DependentLayout
        ohos:height="match_parent"
        ohos:width="match_parent">

    </DependentLayout>

</com.blankj.swipepanel.SwipePanel>
```
#### 版本迭代
- v1.0.0



